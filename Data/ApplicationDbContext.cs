using JobBoard.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JobBoard.Data
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Classification <-> ClassificationKeyword <-> Keyword many-to-many relationship
            builder.Entity<ClassificationKeyword>()
                .HasKey(ck => new {ck.ClassificationId, ck.KeywordId});
            builder.Entity<ClassificationKeyword>()
                .HasOne(ck => ck.Classification)
                .WithMany(k => k.ClassificationKeywords)
                .HasForeignKey(ck => ck.ClassificationId);
            builder.Entity<ClassificationKeyword>()
                .HasOne(ck => ck.Keyword)
                .WithMany(k => k.ClassificationKeywords)
                .HasForeignKey(ck => ck.KeywordId);
            
            // TODO: Add a jobtag relationship?
//            builder.Entity<JobTag>()
//                .HasKey(jt => new {jt.TagId, jt.JobId});
//            builder.Entity<ClassificationKeyword>()
//                .HasOne(ck => ck.Classification)
//                .WithMany(k => k.ClassificationKeywords)
//                .HasForeignKey(ck => ck.ClassificationId);
//            builder.Entity<ClassificationKeyword>()
//                .HasOne(ck => ck.Keyword)
//                .WithMany(k => k.ClassificationKeywords)
//                .HasForeignKey(ck => ck.KeywordId);
        }

        public DbSet<Source> Source { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Job> Job { get; set; }
        public DbSet<Classification> Classification { get; set; }
        public DbSet<Keyword> Keyword { get; set; }
        
        public DbSet<JobClassification> JobClassification { get; set; }
        
        public DbSet<Tag> Tag { get; set; }
        public DbSet<JobTag> JobTag { get; set; }
    }
}