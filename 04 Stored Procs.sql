CREATE PROCEDURE [dbo].[jb_PopulateKeyword]
    @Keyword NVARCHAR(32),
    @Classification NVARCHAR(64)
AS
BEGIN
    DECLARE @Id UNIQUEIDENTIFIER
    SET @Id = NEWID();
    
    INSERT INTO [dbo].Keyword
    (Id, Word, CreatedAt, UpdatedAt)
    VALUES
    (@Id, @Keyword, getdate(), getdate())

    INSERT INTO [dbo].[ClassificationKeyword]
    (ClassificationId, KeywordId)
    SELECT
        (SELECT Id FROM [dbo].[Classification] WHERE Name = @Classification),
        Id FROM [dbo].[Keyword]
        WHERE Id = @Id
END
GO

EXEC jb_PopulateKeyword @Keyword = 'orm', @Classification = 'Software Engineering'
EXEC jb_PopulateKeyword @Keyword = 'mvc', @Classification = 'Software Engineering'
