-- Used in Classification.cs and ClassificationService.cs
SELECT
    k.Id,
    k.Word,
    c.Name,
    ck.ClassificationId,
    k.CreatedAt,
    k.UpdatedAt
FROM [JobBoard].[dbo].[Keyword] k
         INNER JOIN [JobBoard].[dbo].ClassificationKeyword CK on k.Id = CK.KeywordId
         INNER JOIN [JobBoard].[dbo].Classification C on CK.ClassificationId = C.Id

SELECT * FROM dbo.Job
WHERE TITLE LIKE '%security%';

SELECT * FROM Classification
SELECT * FROM ClassificationKeyword

INSERT INTO [dbo].Tag
    (Id, Name)
VALUES 
    (newid(), 'javascript'),
    (newid(), 'html'),
    (newid(), 'css'),
    (newid(), 'sql'),
    (newid(), 'python'),
    (newid(), 'java'),
    (newid(), 'bash'),
    (newid(), 'shell'),
    (newid(), 'powershell'),
    (newid(), 'c#'),
    (newid(), 'php'),
    (newid(), 'c++'),
    (newid(), 'typescript'),
    (newid(), 'c'),
    (newid(), 'ruby'),
    (newid(), 'go'),
    (newid(), 'assembly'),
    (newid(), 'swift'),
    (newid(), 'kotlin'),
    (newid(), 'r'),
    (newid(), 'vba'),
    (newid(), 'objective-c'),
    (newid(), 'scala'),
    (newid(), 'rust'),
    (newid(), 'dart'),
    (newid(), 'elixir'),
    (newid(), 'clojure'),
    (newid(), 'webassembly')

INSERT INTO [dbo].Tag
    (Id, Name)
VALUES
    (newid(), 'vbscript'),
    (newid(), 'matlab'),
    (newid(), 'perl'),
    (newid(), 'cobol'),
    (newid(), 'pascal'),
    (newid(), 'delphi'),
    (newid(), 'r'),
    (newid(), 'groovy'),
    (newid(), 'f#'),
    (newid(), 'lisp'),
    (newid(), 'scheme'),
    (newid(), 'erlang'),
    (newid(), 'haskell'),
    (newid(), 'pl/sql'),
    (newid(), 'd'),
    (newid(), 'fortran'),
    (newid(), 't-sql'),
    (newid(), 'vba'),
    (newid(), 'prolog'),
    (newid(), 'tcl'),
    (newid(), 'julia'),
    (newid(), 'lua'),
    (newid(), 'elm'),
    (newid(), 'smalltalk'),
    (newid(), 'coldfusion'),
    (newid(), 'golang')
    
INSERT INTO [dbo].Tag
    (Id, Name, Type)
VALUES
    (newid(), 'docker', 'DevOps'),
    (newid(), 'jenkins', 'DevOps'),
    (newid(), 'vagrant', 'DevOps'),
    (newid(), 'querysurge', 'DevOps'),
    (newid(), 'pagerduty', 'DevOps'),
    (newid(), 'prometheus', 'DevOps'),
    (newid(), 'snort', 'DevOps'),
    (newid(), 'splunk', 'DevOps'),
    (newid(), 'nagios', 'DevOps'),
    (newid(), 'sumologic', 'DevOps'),
    (newid(), 'overops', 'DevOps'),
    (newid(), 'consul', 'DevOps'),
    (newid(), 'saltstack', 'DevOps'),
    (newid(), 'cfengine', 'DevOps'),
    (newid(), 'artifactory', 'DevOps'),
    (newid(), 'capistrano', 'DevOps'),
    (newid(), 'monit', 'DevOps'),
    (newid(), 'ansible', 'DevOps'),
    (newid(), 'codeclimate', 'DevOps'),
    (newid(), 'icinga', 'DevOps'),
    (newid(), 'relic', 'DevOps'),
    (newid(), 'juju', 'DevOps'),
    (newid(), 'productionmap', 'DevOps'),
    (newid(), 'scalyr', 'DevOps'),
    (newid(), 'rudder', 'DevOps'),
    (newid(), 'puppet', 'DevOps'),
    (newid(), 'graylog', 'DevOps'),
    (newid(), 'upguard', 'DevOps'),
    (newid(), 'zookeeper', 'DevOps'),
    (newid(), 'chef', 'DevOps')

INSERT INTO [dbo].Tag
    (Id, Name, Type)
VALUES
    (newid(), 'oracle', 'Database'),
    (newid(), 'sql server', 'Database'),
    (newid(), 'sybase', 'Database'),
    (newid(), 'teradata', 'Database'),
    (newid(), 'adabas', 'Database'),
    (newid(), 'mysql', 'Database'),
    (newid(), 'postgresql', 'Database'),
    (newid(), 'postgres', 'Database'),
    (newid(), 'filemaker', 'Database'),
    (newid(), 'access', 'Database'),
    (newid(), 'informix', 'Database'),
    (newid(), 'sqlite', 'Database'),
    (newid(), 'rds', 'Database'),
    (newid(), 'mongo', 'Database'),
    (newid(), 'mongodb', 'Database'),
    (newid(), 'redis', 'Database'),
    (newid(), 'couchdb', 'Database'),
    (newid(), 'neo4j', 'Database'),
    (newid(), 'orientdb', 'Database'),
    (newid(), 'couchbase', 'Database'),
    (newid(), 'toad', 'Database'),
    (newid(), 'mariadb', 'Database'),
    (newid(), 'informix', 'Database'),
    (newid(), 'arangodb', 'Database'),
    (newid(), 'liquibase', 'Database'),
    (newid(), 'redgate', 'Database'),
    (newid(), 'bigtable', 'Database'),
    (newid(), 'nuodb', 'Database'),
    (newid(), 'cassandra', 'Database'),
    (newid(), 'simpledb', 'Database'),
    (newid(), 'idera', 'Database'),
    (newid(), 'hana', 'Database'),
    (newid(), 'db2', 'Database')

INSERT INTO [dbo].Tag
(Id, Name, Type)
VALUES
(newid(), 'nosql', 'Database')
    
SELECT t.Name, t.Type, j.Link, j.Title
FROM [dbo].[Job] j
    INNER JOIN [dbo].[JobTag] jt ON j.Id = jt.JobId
    INNER JOIN [dbo].[Tag] t ON t.Id = jt.TagId
WHERE j.Title like '%apple%' 

select * from Keyword
inner join ClassificationKeyword CK on Keyword.Id = CK.KeywordId
inner join Classification C on CK.ClassificationId = C.Id
where c.Name = 'Software Engineering'

-- This works. It allows you to specify tags you want to exclude
-- You would just need to add more LIKEs and NOT LIKEs at the bottom
WITH Exclusion_CTE
         (Id, Title, Link, TagList)
         AS (
             SELECT j.Id,
                    j.Title,
                    j.Link,
                    STUFF
                        (
                           (
                               SELECT ', ' + t.Name
                               FROM Tag st
                                   INNER JOIN [dbo].[JobTag] jt ON j.Id = jt.JobId
                                   INNER JOIN [dbo].[Tag] t ON t.Id = jt.TagId
                               WHERE st.Id = jt.TagId
                                     FOR XML PATH('')
                           ), 1, 1, ''
                        ) as TagList
             FROM [dbo].[Job] j
                 INNER JOIN [dbo].[JobTag] jt ON j.Id = jt.JobId
                 INNER JOIN [dbo].[Tag] t ON t.Id = jt.TagId
        )
SELECT DISTINCT Title, Link, TagList
FROM Exclusion_CTE
WHERE TagList LIKE '%sql%'
   AND TagList NOT LIKE '%java%'
   AND TagList NOT LIKE '%docker%'
   AND TagList NOT LIKE '%scala%'
   AND TagList NOT LIKE '%swift%'
