using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JobBoard.Models;
using JobBoard.Services;

namespace JobBoard.Controllers
{
    public class JobController : Controller
    {
        private readonly IJobService _jobService;
        private readonly IClassificationService _classificationService;
        private readonly ITagService _tagService;

        public JobController(IJobService jobService, IClassificationService classificationService, ITagService tagService)
        {
            _jobService = jobService;
            _classificationService = classificationService;
            _tagService = tagService;
        }
        
        public async Task<IActionResult> Index()
        {
           // get items from database
           var jobs = await _jobService.GetJobsAsync();
           // put items in model
           var model = new JobViewModel()
           {
               Jobs = jobs
           };
           // pass model to view and render
           return View(model);
        }
        
        public async Task<IActionResult> ClassifyJob(Guid id)
        {
            await _classificationService.ClassifyAsync(id);
            return Redirect("/Job");
        }
        
        public async Task<IActionResult> TagJob(Guid id)
        {
            var tag = await _tagService.TagJobAsync(id);
            
            if (tag == null)
            {
                return NotFound();
            } 
            
            return Redirect("/Job");
        }
    }
}