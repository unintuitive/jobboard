using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JobBoard.Services;
using JobBoard.Models;

namespace JobBoard.Controllers
{
    public class ClassificationController : Controller
    {

        private readonly IClassificationService _classificationService;

        public ClassificationController(IClassificationService classificationService)
        {
            _classificationService = classificationService;
        }
        
        public async Task<IActionResult> Index()
        {
            var classifications = _classificationService.GetClassificationsAsync();
            
            var model = new ClassificationViewModel()
            {
                Classifications = await classifications
            };

            return View(model);
        }

//        public async Task<IActionResult> Classify()
//        {
//            var result = await _classificationService.ClassifyAsync();
//
//            var model = new ClassificationViewModel()
//            {
//                Documents = result
//            };
//
//            return View(model);
//        }
    }
}