using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JobBoard.Services;
using JobBoard.Models;

namespace JobBoard.Controllers
{
    public class KeywordController : Controller
    {
        private readonly IKeywordService _keywordService;

        public KeywordController(IKeywordService keywordService)
        {
            _keywordService = keywordService;
        }
        
        public async Task<IActionResult> Index()
        {
            var keywords = _keywordService.GetKeywordsAsync();

            var model = new KeywordViewModel()
            {
                Keywords = await keywords
            };

            return View(model);
        }
    }
}