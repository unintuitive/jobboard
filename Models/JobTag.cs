using System;
using System.Threading.Tasks;
using JobBoard.Data;

namespace JobBoard.Models
{
    public class JobTag
    {
        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        public Guid TagId { get; set; }
    
//        public static Task AddJobTagAsync(Guid jobId, Guid tagId, ApplicationDbContext context)
        public static Task AddJobTagAsync(Guid jobId, Guid tagId, ApplicationDbContext context)
        {
           var jt = new JobTag()
           {
               Id = new Guid(),
               TagId = tagId,
               JobId = jobId
           };
           context.JobTag.Add(jt);
           return context.SaveChangesAsync();
        }
    }
}
