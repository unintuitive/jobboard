using System;
using System.Collections.Generic;

namespace JobBoard.Models
{
    public class Tag
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        
        public ICollection<JobTag> JobTags { get; set; }
    }
}