using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

using JobBoard.Models;

namespace JobBoard.Models
{
    public class ManageUsersViewModel : IdentityDbContext<IdentityUser>
    {
        public IdentityUser[] Administrators { get; set; }
        public IdentityUser[] Everyone { get; set; }
    }
}