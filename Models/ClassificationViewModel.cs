using System;
using JobBoard.Data;

namespace JobBoard.Models
{
    public class ClassificationViewModel
    {
        public Classification[] Classifications { get; set; }
        public Document[] Documents { get; set; }
    }
}