using System;

namespace JobBoard.Models
{
    public class ClassificationKeyword
    {
        public Guid ClassificationId { get; set; }
        public Classification Classification { get; set; }
        
        public Guid KeywordId { get; set; }
        public Keyword Keyword { get; set; }

    }
}