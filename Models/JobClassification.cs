using System;
using System.Threading.Tasks;
using JobBoard.Data;

namespace JobBoard.Models
{
    public class JobClassification
    {
        public Guid Id { get; set; }
        
        public Guid JobId { get; set; }
        public Job Job { get; set; }

        public Guid ClassificationId { get; set; }
        public Classification Classification { get; set; }

       public static Task AddJobClassificationAsync(Guid jobId, Guid classificationId, ApplicationDbContext context)
       {
           var jc = new JobClassification()
           {
               Id = new Guid(),
               ClassificationId = classificationId,
               JobId = jobId
           };
           context.JobClassification.Add(jc);
           return context.SaveChangesAsync();
       }
    }
}