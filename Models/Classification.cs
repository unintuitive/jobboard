using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using JobBoard.Data;

namespace JobBoard.Models
{
    public class Classification
    {
       public Guid Id { get; set; } 
       public String Name { get; set; }
       public bool Active { get; set; }
       public DateTime CreatedAt { get; set; }
       public DateTime UpdatedAt { get; set; }
       
       public ICollection<Keyword> Keywords { get; set; }
       public ICollection<ClassificationKeyword> ClassificationKeywords { get; set; }
       
       public ICollection<Job> Jobs { get; set; }
       public ICollection<JobClassification> JobClassifications { get; set; }

       public static List<Document> GetTrainingCorpusAsync(ApplicationDbContext context)
       {
           var results = context.Set<Keyword>()
               .FromSql(
                   "SELECT k.Id, k.Word, c.Name, ck.ClassificationId, k.CreatedAt, k.UpdatedAt FROM [JobBoard].[dbo].[Keyword] k INNER JOIN [JobBoard].[dbo].ClassificationKeyword CK on k.Id = CK.KeywordId INNER JOIN [JobBoard].[dbo].Classification C on CK.ClassificationId = C.Id")
               .Include(k => k.Classification)
               .ToListAsync();
           
           var docs = new List<Document>();
           foreach (var res in results.Result)
           {
               var doc = new Document(res.Classification.Name, res.Word);
               docs.Add(doc);
           }

           return docs;
       }
    }
    
    public class Document 
    {
        public string Class { get; set; }
        public string Text { get; set; }
        
        public Document(string @class, string text)
        {
            Class = @class;
            Text = text;
        }
    }
}