using System;
using System.Collections.Generic;

namespace JobBoard.Models
{
    public class Job
    {
       public Guid Id { get; set; } 
       public string Link { get; set; }
       public string Title { get; set; }
       public string Content { get; set; }
       public string Description { get; set; }
       public bool Active { get; set; }
       public string PublishedAt { get; set; }
       public DateTime CreatedAt { get; set; }
       public DateTime UpdatedAt { get; set; }
       
       public int SourceId { get; set; }
       public Source Source { get; set; }
       
       public Classification Classification { get; set; }
       
       public ICollection<ClassificationKeyword> ClassificationKeywords { get; set; }
    }
    
}