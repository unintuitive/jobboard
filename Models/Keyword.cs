using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using JobBoard.Data;

namespace JobBoard.Models
{
    public class Keyword
    {
       public Guid Id { get; set; } 
       public String Word { get; set; }
       public DateTime CreatedAt { get; set; }
       public DateTime UpdatedAt { get; set; }
       
       public Classification Classification { get; set; }
       public ICollection<ClassificationKeyword> ClassificationKeywords { get; set; }

       public static Task<Keyword[]> SQLKeywordsAsync(ApplicationDbContext context)
       {
           var results = context.Set<Keyword>()
               .FromSql("SELECT k.Id, k.Word, ck.ClassificationId, c.Name, k.CreatedAt, k.UpdatedAt FROM [JobBoard].[dbo].[Keyword] k INNER JOIN [JobBoard].[dbo].ClassificationKeyword CK on k.Id = CK.KeywordId INNER JOIN [JobBoard].[dbo].Classification C on CK.ClassificationId = C.Id")
               .Include(k => k.Classification)
               .OrderBy(c => c.Classification.Name)
               .ToArrayAsync();
           return results;
       }
    }
}