DECLARE
    @KeywordId uniqueidentifier,
    @ClassificationId uniqueidentifier

INSERT INTO [dbo].[Classification]
    (Id, Name, Active, CreatedAt, UpdatedAt)
VALUES (newid(), 'Security', 1, GETDATE(), GETDATE())

SELECT * FROM [dbo].[Classification];

INSERT INTO [dbo].[Keyword]
(Id, Word, CreatedAt, UpdatedAt)
VALUES
(NEWID(), 'DOD', GETDATE(), GETDATE()),
(NEWID(), 'NIST', GETDATE(), GETDATE()),
(NEWID(), 'IDS', GETDATE(), GETDATE()),
(NEWID(), 'IPS', GETDATE(), GETDATE()),
(NEWID(), 'IDS/IPS', GETDATE(), GETDATE()),
(NEWID(), 'SIEM', GETDATE(), GETDATE()),
(NEWID(), 'NOC', GETDATE(), GETDATE()),
(NEWID(), 'splunk', GETDATE(), GETDATE()),
(NEWID(), 'burpsuite', GETDATE(), GETDATE()),
(NEWID(), 'metasploit', GETDATE(), GETDATE()),
(NEWID(), 'breach', GETDATE(), GETDATE()),
(NEWID(), 'contingency', GETDATE(), GETDATE()),
(NEWID(), 'violations', GETDATE(), GETDATE()),
(NEWID(), 'mission', GETDATE(), GETDATE()),
(NEWID(), 'detection', GETDATE(), GETDATE()),
(NEWID(), 'compliance', GETDATE(), GETDATE()),
(NEWID(), 'infosec', GETDATE(), GETDATE()),
(NEWID(), 'security', GETDATE(), GETDATE()),
(NEWID(), 'secure', GETDATE(), GETDATE()),
(NEWID(), 'cyber', GETDATE(), GETDATE()),
(NEWID(), 'cybersecurity', GETDATE(), GETDATE()),
(NEWID(), 'cyber-security', GETDATE(), GETDATE()),
(NEWID(), 'incident', GETDATE(), GETDATE()),
(NEWID(), 'risk', GETDATE(), GETDATE()),
(NEWID(), 'risk-based', GETDATE(), GETDATE()),
(NEWID(), 'privacy', GETDATE(), GETDATE()),
(NEWID(), 'mitigate', GETDATE(), GETDATE()),
(NEWID(), 'threat', GETDATE(), GETDATE()),
(NEWID(), 'threats', GETDATE(), GETDATE()),
(NEWID(), 'vulnerability', GETDATE(), GETDATE()),
(NEWID(), 'vulnerabilities', GETDATE(), GETDATE()),
(NEWID(), 'vulns', GETDATE(), GETDATE()),
(NEWID(), 'penetration', GETDATE(), GETDATE()),
(NEWID(), 'pentesting', GETDATE(), GETDATE()),
(NEWID(), 'pentester', GETDATE(), GETDATE()),
(NEWID(), 'exploitation', GETDATE(), GETDATE()),
(NEWID(), 'exploit', GETDATE(), GETDATE()),
(NEWID(), 'exploits', GETDATE(), GETDATE()),
(NEWID(), 'malware', GETDATE(), GETDATE());

SELECT Id FROM [dbo].[Classification] WHERE Name = 'Security';

INSERT INTO [dbo].[ClassificationKeyword]
(ClassificationId, KeywordId)
SELECT
       (SELECT Id FROM [dbo].[Classification] WHERE Name = 'Security'),
       Id FROM [dbo].[Keyword];

DECLARE @tempID uniqueidentifier = (SELECT NEWID());
SELECT * FROM ClassificationKeyword
INSERT INTO [dbo].[Keyword]
    (Id, Word, CreatedAt, UpdatedAt)
VALUES
    (newid(), 'docker', GETDATE(), GETDATE()),
    (newid(), 'kubernetes', GETDATE(), GETDATE()),
    (newid(), 'development', GETDATE(), GETDATE()),
    (newid(), 'agile', GETDATE(), GETDATE()),
    (newid(), 'scrum', GETDATE(), GETDATE()),
    (newid(), 'agile/scrum', GETDATE(), GETDATE()),
    (newid(), 'waterfall', GETDATE(), GETDATE()),
    (newid(), 'testing', GETDATE(), GETDATE()),
    (newid(), 'languages', GETDATE(), GETDATE()),
    (newid(), 'coding', GETDATE(), GETDATE()),
    (newid(), 'developer', GETDATE(), GETDATE()),
    (newid(), 'asp', GETDATE(), GETDATE()),
    (newid(), 'rails', GETDATE(), GETDATE()),
    (newid(), 'laravel', GETDATE(), GETDATE()),
    (newid(), 'nosql', GETDATE(), GETDATE()),
    (newid(), 'git', GETDATE(), GETDATE()),
    (newid(), 'accessibility', GETDATE(), GETDATE()),
    (newid(), 'mvc', GETDATE(), GETDATE()),
    (newid(), 'php', GETDATE(), GETDATE()),
    (newid(), 'javascript', GETDATE(), GETDATE()),
    (newid(), 'angular', GETDATE(), GETDATE()),
    (newid(), 'Kafka', GETDATE(), GETDATE()),
    (newid(), 'RabbitMQ', GETDATE(), GETDATE()),
    (newid(), 'DevOps', GETDATE(), GETDATE()),
    (newid(), 'microservices', GETDATE(), GETDATE()),
    (newid(), 'micro-services', GETDATE(), GETDATE()),
    (newid(), 'pub-sub', GETDATE(), GETDATE()),
    (newid(), 'code', GETDATE(), GETDATE()),
    (newid(), 'debugging', GETDATE(), GETDATE()),
    (newid(), 'performance', GETDATE(), GETDATE()),
    (newid(), 'functions', GETDATE(), GETDATE()),
    (newid(), 'SSIS', GETDATE(), GETDATE()),
    (newid(), 'Tableau', GETDATE(), GETDATE()),
    (newid(), 'queries', GETDATE(), GETDATE()),
    (newid(), 'develop', GETDATE(), GETDATE()),
    (newid(), 'testable', GETDATE(), GETDATE()),
    (newid(), 'maintainable', GETDATE(), GETDATE()),
    (newid(), 'multi-threading', GETDATE(), GETDATE()),
    (newid(), 'concurrency', GETDATE(), GETDATE()),
    (newid(), 'algorithms', GETDATE(), GETDATE()),
    (newid(), 'neural', GETDATE(), GETDATE()),
    (newid(), 'distributed', GETDATE(), GETDATE()),
    (newid(), 'multicore', GETDATE(), GETDATE());

select * from Classification;

-- INSERT INTO [dbo].[Classification]
-- (Id, Name, Active, CreatedAt, UpdatedAt)
-- VALUES (newid(), 'Software Engineering', 1, GETDATE(), GETDATE())
-- D02E13A6-FC1B-4B37-B2BB-4576E338AB0

INSERT INTO [dbo].[ClassificationKeyword]
    (ClassificationId, KeywordId)
SELECT
    (SELECT Id FROM [dbo].[Classification] WHERE Name = 'Software Engineering'),
    Id FROM [dbo].[Keyword]
    WHERE
          Word IN (
                  'docker',
                  'kubernetes',
                  'development',
                  'agile',
                  'scrum',
                  'agile/scrum',
                  'waterfall',
                  'testing',
                  'languages',
                  'coding',
                  'developer',
                  'asp',
                  'rails',
                  'laravel',
                  'nosql',
                  'git',
                  'accessibility',
                  'mvc',
                  'php',
                  'javascript',
                  'angular',
                  'Kafka',
                  'RabbitMQ',
                  'DevOps',
                  'microservices',
                  'micro-services',
                  'pub-sub',
                  'code',
                  'debugging',
                  'performance',
                  'functions',
                  'SSIS',
                  'Tableau',
                  'queries',
                  'develop',
                  'testable',
                  'maintainable',
                  'multi-threading',
                  'concurrency',
                  'algorithms',
                  'neural',
                  'distributed',
                  'multicore'
              );

INSERT INTO [dbo].Keyword
(id, Word, CreatedAt, UpdatedAt)
VALUES
(NEWID(), 'api', getdate(), getdate()),
(NEWID(), 'developing', getdate(), getdate()),
(NEWID(), 'features', getdate(), getdate()),
(NEWID(), 'production', getdate(), getdate()),
(NEWID(), 'legacy', getdate(), getdate()),
(NEWID(), 'framework', getdate(), getdate()),
(NEWID(), 'frontend', getdate(), getdate()),
(NEWID(), 'backend', getdate(), getdate())

INSERT INTO [dbo].[ClassificationKeyword]
(ClassificationId, KeywordId)
SELECT
    (SELECT Id FROM [dbo].[Classification] WHERE Name = 'Software Engineering'),
    Id FROM [dbo].[Keyword]
WHERE
        Word IN (
                 'api',
                 'developing',
                 'features',
                 'production',
                 'legacy',
                 'framework',
                 'frontend',
                 'backend')

INSERT INTO [dbo].[ClassificationKeyword]
(ClassificationId, KeywordId)
SELECT
    (SELECT Id FROM [dbo].[Classification] WHERE Name = 'Software Engineering'),
    Id FROM [dbo].[Keyword]
WHERE
        Word IN (
                 'frontend',
                 'backend')
