USE JobBoard;
GO

DROP TABLE IF EXISTS [dbo].[SyndicationFeed]
DROP TABLE IF EXISTS [dbo].Source
GO
CREATE TABLE [dbo].[SyndicationSource]
(
    [Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    [CategoryId] INT NOT NULL,
    [Name] NVARCHAR(255) NOT NULL,
    [URL] NVARCHAR(255) NOT NULL,
    [Active] BIT NOT NULL DEFAULT 1,
    [CreatedAt] DATE DEFAULT GETDATE(),
    [UpdatedAt] DATE NULL
);
GO

DROP TABLE IF EXISTS [dbo].[FeedCategory]
GO
CREATE TABLE [dbo].[FeedCategory]
(
    [Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    [Name] NVARCHAR(255) NOT NULL,
    [CreatedAt] DATE DEFAULT GETDATE(),
    [UpdatedAt] DATE NULL
);
GO

-- ALTER TABLE [dbo].Source
--     ADD CONSTRAINT FK_SyndicationSource_FeedCategory
--     FOREIGN KEY(CategoryId)
--     REFERENCES [dbo].[FeedCategory];

INSERT INTO [dbo].[Category]
    (Name, CreatedAt)
VALUES
    ('Jobs', GETDATE()),
    ('Remote Jobs', GETDATE()),
    ('Crypto Jobs', GETDATE()),
    ('Framework Jobs', GETDATE()),
    ('Language Jobs', GETDATE())

DECLARE @JobFeedJson NVARCHAR(MAX)
SET @JobFeedJson = '
[
    {
        "name": "WeWorkRemotely",
        "url": "https://weworkremotely.com/remote-jobs.rss",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "GitHub Remote",
        "url": "https://jobs.github.com/positions.atom?description=&location=Remote",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Python.org Jobs",
        "url": "https://www.python.org/jobs/feed/rss/",
        "category": "lang-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Django Jobs",
        "url": "https://djangogigs.com/feeds/gigs/",
        "category": "framework-jobs",
        "show_in_homepage": true
    },
    {
        "name": "RoR Jobs",
        "url": "https://www.rorjobs.com/jobs.rss",
        "category": "framework-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Angular Jobs",
        "url": "https://angularjobs.com/wpjobboard/xml/rss/",
        "category": "framework-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Ember Jobs",
        "url": "https://jobs.emberjs.com/jobs.rss",
        "category": "framework-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Drupal Jobs",
        "url": "https://jobs.drupal.org/all-jobs/feed",
        "category": "framework-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Crunchboard Jobs",
        "url": "http://feeds.feedburner.com/CrunchboardJobs?format=xml",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "StackOverflow Jobs",
        "url": "https://stackoverflow.com/jobs/feed",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "RemoteOk",
        "url": "https://remoteok.io/remote-jobs.rss",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Functional Jobs",
        "url": "https://functionaljobs.com/jobs/?format=rss",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "FOSS Jobs",
        "url": "https://www.fossjobs.net/rss/all/",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "RemotePython.com",
        "url": "https://www.remotepython.com/latest/jobs/feed/",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "CryptoJobsList",
        "url": "https://cryptojobslist.com/jobs.rss",
        "category": "crypto-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Cryptocurrency Jobs",
        "url": "https://cryptocurrencyjobs.co/index.xml",
        "category": "crypto-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Core Intuition Jobs",
        "url": "http://jobs.coreint.org/rss.xml",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Adafruit Job Boards",
        "url": "https://jobs.adafruit.com/wpjobboard/xml/rss/?query=&amp;location=&amp;type%5B%5D=7",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Authentic Jobs",
        "url": "https://authenticjobs.com/rss/custom.php?remote=1",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "CodePen Job Listings",
        "url": "https://codepen.io/jobs/feed",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Dribbble",
        "url": "https://dribbble.com/jobs.rss?anywhere=true&amp;location=Anywhere",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Freelancer",
        "url": "https://www.freelancer.com/rss.xml",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Golang / Go Jobs",
        "url": "https://www.golangprojects.com/rss.xml",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "HasJob",
        "url": "https://hasjob.co/feed",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Indeed",
        "url": "http://rss.indeed.com/rss",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Jobbatical",
        "url": "https://feeds.feedburner.com/jobbatical-jobs",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Jobspresso",
        "url": "https://jobspresso.co/feed/?post_type=job_listing",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Krop",
        "url": "http://www.krop.com/services/feeds/rss/latest/",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Lara Jobs",
        "url": "https://larajobs.com/feed",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Mashable Jobs",
        "url": "http://jobs.mashable.com/jobs/search/results?format=rss",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "NTEN",
        "url": "https://www.nten.org/feed/?post_type=job",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Reddit (/r/remotejs)",
        "url": "https://www.reddit.com/r/remotejs/.rss",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Reddit (/r/remotephp)",
        "url": "https://www.reddit.com/r/remotephp/.rss",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Reddit (/r/remotepython)",
        "url": "https://www.reddit.com/r/remotepython/.rss",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Remote Work Hub",
        "url": "https://remoteworkhub.com/feed/?post_type=job",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Remote.co",
        "url": "https://remote.co/feed/?post_type=job_listing",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Remotive",
        "url": "https://remotive.io/feed/?post_type=job_listing",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "Stack Overflow (remote)",
        "url": "https://stackoverflow.com/jobs/feed?r=True",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "StartUpers",
        "url": "http://feeds.feedburner.com/startup-jobs",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Virtual Vocations",
        "url": "https://www.virtualvocations.com/jobs/rss",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Berlin Startup Jobs",
        "url": "http://berlinstartupjobs.com/feed/",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Vue Jobs",
        "url": "https://vuejobs.com/feed",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "42jobs React",
        "url": "https://www.42jobs.io/react/jobs.rss",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "42jobs Laravel",
        "url": "https://www.42jobs.io/laravel/jobs.rss",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "42jobs Rails",
        "url": "https://www.42jobs.io/rails/jobs.rss",
        "show_in_homepage": true
    },
    {
        "name": "42jobs Android",
        "url": "https://www.42jobs.io/android/jobs.rss",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "WhereverJobs",
        "url": "https://whereverjobs.com/rss",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "JobMote",
        "url": "http://jobmote.com/jobs/feed.rss",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "WordPress Jobs",
        "url": "https://jobs.wordpress.net/feed/",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "RemoteJobr",
        "url": "https://remotejobr.com/feed?category=1&format=rss",
        "category": "remote-jobs",
        "show_in_homepage": true
    },
    {
        "name": "WP Hired",
        "url": "http://www.wphired.com/jobs/feed/",
        "category": "jobs",
        "show_in_homepage": true
    },
    {
        "name": "Landing.jobs",
        "url": "https://landing.jobs/feed?remote=true",
        "category": "remote-jobs",
        "show_in_homepage": false
    }
]
';

WITH
    JobFeedJson_CTE
    (Name, URL, Category)
    AS
    (
        SELECT [Name], URL, Category
        FROM OPENJSON(@JobFeedJson)
        WITH
        (
            [Name]   NVARCHAR(255) '$.name' ,
            [URL]    NVARCHAR(255) '$.url',
            Category NVARCHAR(255) '$.category'
        )
    )
INSERT INTO [dbo].Source
(Name, URL, CategoryId, Active, CreatedAt, UpdatedAt)
SELECT
    [Name],
    [URL],
    CASE
        WHEN Category = 'jobs' THEN (
            SELECT Id
            FROM [dbo].Category
            WHERE [Name] = 'Jobs')
        WHEN Category = 'remote-jobs' THEN (
            SELECT Id
            FROM [dbo].Category
            WHERE [Name] = 'Remote Jobs')
        WHEN Category = 'crypto-jobs' THEN (
            SELECT Id
            FROM [dbo].Category
            WHERE [Name] = 'Crypto Jobs')
        WHEN Category = 'framework-jobs' THEN (
            SELECT Id
            FROM [dbo].Category
            WHERE [Name] = 'Framework Jobs')
        WHEN Category = 'lang-jobs' THEN (
            SELECT Id
            FROM [dbo].Category
            WHERE [Name] = 'Language Jobs')
        ELSE
            (SELECT Id
            FROM [dbo].Category
            WHERE [Name] = 'Jobs')
    END AS Category_Id,
    1,
    GETDATE(),
    GETDATE()
FROM JobFeedJson_CTE

SELECT * FROM [dbo].Source

select * from dbo.Job
where title like '%Seeq%'
--
create table job (id int null);
-- create table source (id int null);
-- create table Category (id int null);

DECLARE
    @KeywordId uniqueidentifier,
    @ClassificationId uniqueidentifier
INSERT INTO [dbo].[Classification]
(Id, Name, Active, CreatedAt, UpdatedAt, JobId)
VALUES (newid(), 'Security', 1, GETDATE(), GETDATE(), newid())
set @ClassificationId = SCOPE_IDENTITY()
