#pragma checksum "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ad72b8c03c43925dc247d45efb0eb7fc0a351261"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ManageUsers_Index), @"mvc.1.0.view", @"/Views/ManageUsers/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/ManageUsers/Index.cshtml", typeof(AspNetCore.Views_ManageUsers_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\eric\Projects\DotNet\JobBoard\Views\_ViewImports.cshtml"
using JobBoard;

#line default
#line hidden
#line 2 "C:\Users\eric\Projects\DotNet\JobBoard\Views\_ViewImports.cshtml"
using JobBoard.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ad72b8c03c43925dc247d45efb0eb7fc0a351261", @"/Views/ManageUsers/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2bec7572acf45408433c84b57d98119fbebe1cb7", @"/Views/_ViewImports.cshtml")]
    public class Views_ManageUsers_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ManageUsersViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(29, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
  
    ViewData["Title"] = "Manage Users";

#line default
#line hidden
            BeginContext(79, 6, true);
            WriteLiteral("\r\n<h2>");
            EndContext();
            BeginContext(86, 17, false);
#line 7 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(103, 154, true);
            WriteLiteral("</h2>\r\n<h3>Administrators</h3>\r\n<table class=\"table\">\r\n    <thead>\r\n    <tr>\r\n        <td>ID</td>\r\n        <td>Email</td>\r\n    </tr>\r\n    </thead>\r\n    \r\n");
            EndContext();
#line 17 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
     foreach (var user in Model.Administrators)
    {

#line default
#line hidden
            BeginContext(313, 30, true);
            WriteLiteral("        <tr>\r\n            <td>");
            EndContext();
            BeginContext(344, 7, false);
#line 20 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
           Write(user.Id);

#line default
#line hidden
            EndContext();
            BeginContext(351, 23, true);
            WriteLiteral("</td>\r\n            <td>");
            EndContext();
            BeginContext(375, 10, false);
#line 21 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
           Write(user.Email);

#line default
#line hidden
            EndContext();
            BeginContext(385, 23, true);
            WriteLiteral("</td>\r\n        </tr> \r\n");
            EndContext();
#line 23 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
    }

#line default
#line hidden
            BeginContext(415, 153, true);
            WriteLiteral("</table>\r\n\r\n<h3>Everyone</h3>\r\n<table class=\"table\">\r\n    <thead>\r\n    <tr>\r\n        <td>ID</td>\r\n        <td>Email</td>\r\n    </tr>\r\n    </thead>\r\n    \r\n");
            EndContext();
#line 35 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
     foreach (var user in Model.Everyone)
    {

#line default
#line hidden
            BeginContext(618, 30, true);
            WriteLiteral("        <tr>\r\n            <td>");
            EndContext();
            BeginContext(649, 7, false);
#line 38 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
           Write(user.Id);

#line default
#line hidden
            EndContext();
            BeginContext(656, 23, true);
            WriteLiteral("</td>\r\n            <td>");
            EndContext();
            BeginContext(680, 10, false);
#line 39 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
           Write(user.Email);

#line default
#line hidden
            EndContext();
            BeginContext(690, 23, true);
            WriteLiteral("</td>\r\n        </tr> \r\n");
            EndContext();
#line 41 "C:\Users\eric\Projects\DotNet\JobBoard\Views\ManageUsers\Index.cshtml"
    }

#line default
#line hidden
            BeginContext(720, 12, true);
            WriteLiteral("</table>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ManageUsersViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
