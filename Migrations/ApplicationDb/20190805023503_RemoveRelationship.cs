﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JobBoard.Migrations.ApplicationDb
{
    public partial class RemoveRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_Classification_ClassificationId",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Job_ClassificationId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "ClassificationId",
                table: "Job");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ClassificationId",
                table: "Job",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Job_ClassificationId",
                table: "Job",
                column: "ClassificationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_Classification_ClassificationId",
                table: "Job",
                column: "ClassificationId",
                principalTable: "Classification",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
