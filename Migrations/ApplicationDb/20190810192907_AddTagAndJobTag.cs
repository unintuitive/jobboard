﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JobBoard.Migrations.ApplicationDb
{
    public partial class AddTagAndJobTag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
//            migrationBuilder.AddColumn<Guid>(
//                name: "TagId",
//                table: "Job",
//                nullable: true);

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JobTag",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    JobId = table.Column<Guid>(nullable: false),
                    TagId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobTag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobTag_Job_JobId",
                        column: x => x.JobId,
                        principalTable: "Job",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobTag_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

//            migrationBuilder.CreateIndex(
//                name: "IX_Job_TagId",
//                table: "Job",
//                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_JobTag_JobId",
                table: "JobTag",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_JobTag_TagId",
                table: "JobTag",
                column: "TagId");

//            migrationBuilder.AddForeignKey(
//                name: "FK_Job_Tag_TagId",
//                table: "Job",
//                column: "TagId",
//                principalTable: "Tag",
//                principalColumn: "Id",
//                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
//            migrationBuilder.DropForeignKey(
//                name: "FK_Job_Tag_TagId",
//                table: "Job");

            migrationBuilder.DropTable(
                name: "JobTag");

            migrationBuilder.DropTable(
                name: "Tag");

//            migrationBuilder.DropIndex(
//                name: "IX_Job_TagId",
//                table: "Job");
//
//            migrationBuilder.DropColumn(
//                name: "TagId",
//                table: "Job");
        }
    }
}
