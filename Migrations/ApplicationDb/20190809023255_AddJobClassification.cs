﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JobBoard.Migrations.ApplicationDb
{
    public partial class AddJobClassification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
//            migrationBuilder.AddColumn<Guid>(
//                name: "ClassificationId",
//                table: "Keyword",
//                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ClassificationId",
                table: "Job",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "JobId",
                table: "ClassificationKeyword",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "JobClassification",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    JobId = table.Column<Guid>(nullable: false),
                    ClassificationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobClassification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobClassification_Classification_ClassificationId",
                        column: x => x.ClassificationId,
                        principalTable: "Classification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobClassification_Job_JobId",
                        column: x => x.JobId,
                        principalTable: "Job",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

//            migrationBuilder.CreateIndex(
//                name: "IX_Keyword_ClassificationId",
//                table: "Keyword",
//                column: "ClassificationId");

            migrationBuilder.CreateIndex(
                name: "IX_Job_ClassificationId",
                table: "Job",
                column: "ClassificationId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificationKeyword_JobId",
                table: "ClassificationKeyword",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_JobClassification_ClassificationId",
                table: "JobClassification",
                column: "ClassificationId");

            migrationBuilder.CreateIndex(
                name: "IX_JobClassification_JobId",
                table: "JobClassification",
                column: "JobId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassificationKeyword_Job_JobId",
                table: "ClassificationKeyword",
                column: "JobId",
                principalTable: "Job",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Job_Classification_ClassificationId",
                table: "Job",
                column: "ClassificationId",
                principalTable: "Classification",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

//            migrationBuilder.AddForeignKey(
//                name: "FK_Keyword_Classification_ClassificationId",
//                table: "Keyword",
//                column: "ClassificationId",
//                principalTable: "Classification",
//                principalColumn: "Id",
//                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassificationKeyword_Job_JobId",
                table: "ClassificationKeyword");

            migrationBuilder.DropForeignKey(
                name: "FK_Job_Classification_ClassificationId",
                table: "Job");

//            migrationBuilder.DropForeignKey(
//                name: "FK_Keyword_Classification_ClassificationId",
//                table: "Keyword");

            migrationBuilder.DropTable(
                name: "JobClassification");

//            migrationBuilder.DropIndex(
//                name: "IX_Keyword_ClassificationId",
//                table: "Keyword");

            migrationBuilder.DropIndex(
                name: "IX_Job_ClassificationId",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_ClassificationKeyword_JobId",
                table: "ClassificationKeyword");

//            migrationBuilder.DropColumn(
//                name: "ClassificationId",
//                table: "Keyword");

            migrationBuilder.DropColumn(
                name: "ClassificationId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "JobId",
                table: "ClassificationKeyword");
        }
    }
}
