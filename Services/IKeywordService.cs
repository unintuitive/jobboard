using System.Threading.Tasks;
using JobBoard.Models;

namespace JobBoard.Services
{
    public interface IKeywordService
    {
        Task<Keyword[]> GetKeywordsAsync();
    }
}