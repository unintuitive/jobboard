using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using JobBoard.Data;
using JobBoard.Models;

namespace JobBoard.Services
{
    public class TagService : ITagService
    {
        private readonly ApplicationDbContext _context;

        public TagService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<string>> TagJobAsync(Guid jobId)
        {
            // Pull in an array of tags from the database and convert it to a HashSet
            var tags = await _context.Tag.Select(x => x.Name).ToArrayAsync();
            var tagSet = new HashSet<string>(tags);
            
            // Get the job from the provided id
            var job = await _context.Job.FindAsync(jobId);
            var jobText = job.Title + " " + job.Content + " " + job.Description;
            
            // Sanitize the text of the job, remove stop words, and split it into an array of sentences
            var sentences = Regex.Split(jobText.ToLower(), @"(?<=[\.!\?])\s+");
            var sanitizedSentences = sentences.Select(sentence => StopwordTool.RemoveStopwords(sentence.Sanitize()));

            // We use a hashset here because we only want unique tags 
            var applyTags = new HashSet<string>(); 
            foreach (var sentence in sanitizedSentences)
            {
                var words = sentence.Split(null);
                foreach (var word in words)
                {
                    if (tagSet.Contains(word))
                    {
                        applyTags.Add(word); // Add each match to applyTags
                    }
                }
            }

            // Add each identified tag as an entry to the JobTag model
            foreach (var tag in applyTags)
            {
                var tagId = await _context.Tag.Where(x => x.Name == tag.ToString()).Select(x => x.Id).FirstAsync();
                await JobTag.AddJobTagAsync(jobId, tagId, _context);
            }
            
            return await Task.FromResult(applyTags.ToList());
        }
    }
}