using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using CodeHollow.FeedReader;
using Microsoft.EntityFrameworkCore;
using JobBoard.Data;
using JobBoard.Models;
using Microsoft.Extensions.Logging;

namespace JobBoard.Services
{
    public class FetcherService : IFetcherService
    {
        private readonly ApplicationDbContext _context;
        private readonly ITagService _tagService;

        public FetcherService(ApplicationDbContext context, ITagService tagService)
        {
            _context = context;
            _tagService = tagService;
        }
        
        // Takes a single feed source URL and populates the Job table with the results
        public async Task<Job[]> FetchJobEntriesAsync(Source source)
        {
            var j = new Job()
            {
                Active = true,
                Content = "Hello there",
                CreatedAt = DateTime.Now,
                Id = Guid.NewGuid(),
                Link = "http://mywageslavejob.com",
                PublishedAt = "Long ago in a blah",
                Title = "Wage Slave Intern",
                SourceId = 1,
                UpdatedAt = DateTime.Now
            };

            try
            {
                var data = await FeedReader.ReadAsync(source.URL);
                
                foreach (var item in data.Items)
                {
                    var job = new Job()
                    {
                        Active = true, // Maybe should default to false?
                        Content = item.Content,
                        Description = item.Description,
                        CreatedAt = DateTime.Now,
                        Id = new Guid(), // Converts the entry id string to a guid type
                        Link = item.Link,
                        PublishedAt = item.PublishingDateString,
                        Title = item.Title,
                        SourceId = source.Id,
                        UpdatedAt = DateTime.Now
                    };

                    _context.Job.Add(job);
                    await _context.SaveChangesAsync();
                   
                    // Auto-tag jobs
                    await _tagService.TagJobAsync(job.Id);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: {0}", exception);
            }
            
            return await Task.FromResult(new[] {j});
        }
    }
}