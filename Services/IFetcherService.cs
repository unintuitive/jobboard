using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JobBoard.Data;
using JobBoard.Models;

namespace JobBoard.Services
{
    public interface IFetcherService
    {
        Task<Job[]> FetchJobEntriesAsync(Source source);
    }
}