using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JobBoard.Models;

namespace JobBoard.Services
{
    public interface ITagService
    {
        Task<List<string>> TagJobAsync(Guid jobId);
    }
}
