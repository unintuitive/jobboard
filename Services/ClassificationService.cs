using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using JobBoard.Data;
using JobBoard.Models;
using Microsoft.CodeAnalysis;
using Document = JobBoard.Models.Document;

namespace JobBoard.Services
{
    public class ClassificationService : IClassificationService
    {
        private readonly ApplicationDbContext _context;

        public ClassificationService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Classification[]> GetClassificationsAsync()
        {
            return await _context.Classification.ToArrayAsync();
        }

        public async Task<List<Document>> GetTrainingCorpus(ApplicationDbContext context)
        {
            return Classification.GetTrainingCorpusAsync(context);
        }

        public async Task ClassifyAsync(Guid id)
        {
            // Acquire the necessary data to classify a job
            // TODO: Due to performance concerns, some of these should probably be moved out of ClassifyAsync
            var trainingCorpus = await GetTrainingCorpus(_context);
            var job = await _context.Job.FindAsync(id);
            var classifications = await _context.Classification.ToListAsync();
            
            var classifier = new Classifier(trainingCorpus);
            
            var jobText = job.Title + " " + job.Content + " " + job.Description;
            var sentences = Regex.Split(jobText.Sanitize(), @"(?<=[\.!\?])\s+");
            
            double classificationStrength = 0;
            foreach (var classification in classifications)
            {
                double classificationSum =
                    sentences
                            .Select(sentence => StopwordTool.RemoveStopwords(sentence))
                            .Select(processedSentence => classifier.IsInClassProbability(classification.Name, processedSentence))
                            .OfType<double>()
                            .Sum();
                
                if (classificationSum > 0)
                {
                    classificationStrength = classificationSum / sentences.Length; // Get the average
                }

                if (classificationStrength >= 0.60)
                {
                    Debug.WriteLine(classification.Name);
//                    await JobClassification.AddJobClassificationAsync(job.Id, classification.Id, _context);
                    return;
                }
            }

            // TODO: We don't actually want to return res here. The idea is to apply a classification to a job.
            // Then updating the Job table with the identifier. What we actually /return/ here up for discussion.
        }
    }

    public static class Helpers
    {
        public static List<String> ExtractFeatures(this String text)
        {
            return Regex.Replace(text, "\\p{P}+", "").Split(' ').ToList();
        }
        
        public static string Sanitize(this String raw)
        {
            // Load the string into a function provided by the HTML Agility Pack
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(raw);
            string result = htmlDocument.DocumentNode.InnerText; // Returns only the text
            result = WebUtility.HtmlDecode(result); // Decodes html encoded characters e.g. nbsp; 
            return Regex.Replace(result, @"(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ").ToString();
        }
    }

    class ClassInfo
    {
        public string Name { get; set; }
        public int WordsCount { get; set; }
        public Dictionary<string, int> WordCount { get; set; }
        public int NumberOfDocs { get; set; }

        public int NumberOfOccurencesInTrainDocs(String word)
        {
            if (WordCount.Keys.Contains(word)) return WordCount[word];
            return 0;
        }
        
        public ClassInfo(string name, List<String> trainDocs)
        {
            Name = name;
            var features = trainDocs.SelectMany(x => x.ExtractFeatures());
            WordsCount = features.Count();
            WordCount = features.GroupBy(x => x).ToDictionary(x => x.Key, x => x.Count());
            NumberOfDocs = trainDocs.Count();
        }
        
    }

    class Classifier
    {
        private List<ClassInfo> _classes;
        private int _countOfDocs;
        private int _uniqWordsCount;

        public Classifier(List<Document> train)
        {
            _classes = train
                .GroupBy(x => x.Class)
                .Select(g => new ClassInfo(g.Key, g.Select(x => x.Text).ToList()))
                .ToList();
            _countOfDocs = train.Count();
            _uniqWordsCount = train
                .SelectMany(x => x.Text.Split(' '))
                .GroupBy(x => x)
                .Count();
        }

        public double IsInClassProbability(string className, string text)
        {
            var words = text.ExtractFeatures();
            var classResults = _classes
                .Select(x => new
                {
                    Result = Math.Pow(Math.E,
                        Calc(x.NumberOfDocs, _countOfDocs, words, x.WordsCount, x, _uniqWordsCount)),
                    ClassName = x.Name
                });

            return classResults
                       .Single(x => x.ClassName == className)
                       .Result / classResults
                       .Sum(x => x.Result);
        }

        private static double Calc(double dc, double d, List<String> q, double lc, ClassInfo @class, double v)
        {
            return Math.Log(dc / d) + q.Sum(x => Math.Log((@class.NumberOfOccurencesInTrainDocs(x) + 1) / (v + lc)));
        }
    }
     static class StopwordTool
    {
    /// FROM:  https://www.dotnetperls.com/stopword-dictionary
    /// <summary>
    /// Words we want to remove.
    /// </summary>
    static Dictionary<string, bool> _stops = new Dictionary<string, bool>
    {
        { "a", true },
        { "about", true },
        { "above", true },
        { "across", true },
        { "after", true },
        { "afterwards", true },
        { "again", true },
        { "against", true },
        { "all", true },
        { "almost", true },
        { "alone", true },
        { "along", true },
        { "already", true },
        { "also", true },
        { "although", true },
        { "always", true },
        { "am", true },
        { "among", true },
        { "amongst", true },
        { "amount", true },
        { "an", true },
        { "and", true },
        { "another", true },
        { "any", true },
        { "anyhow", true },
        { "anyone", true },
        { "anything", true },
        { "anyway", true },
        { "anywhere", true },
        { "are", true },
        { "around", true },
        { "as", true },
        { "at", true },
        { "back", true },
        { "be", true },
        { "became", true },
        { "because", true },
        { "become", true },
        { "becomes", true },
        { "becoming", true },
        { "been", true },
        { "before", true },
        { "beforehand", true },
        { "behind", true },
        { "being", true },
        { "below", true },
        { "beside", true },
        { "besides", true },
        { "between", true },
        { "beyond", true },
        { "bill", true },
        { "both", true },
        { "bottom", true },
        { "but", true },
        { "by", true },
        { "call", true },
        { "can", true },
        { "cannot", true },
        { "cant", true },
        { "co", true },
        { "computer", true },
        { "con", true },
        { "could", true },
        { "couldnt", true },
        { "cry", true },
        { "de", true },
        { "describe", true },
        { "detail", true },
        { "do", true },
        { "done", true },
        { "down", true },
        { "due", true },
        { "during", true },
        { "each", true },
        { "eg", true },
        { "eight", true },
        { "either", true },
        { "eleven", true },
        { "else", true },
        { "elsewhere", true },
        { "empty", true },
        { "enough", true },
        { "etc", true },
        { "even", true },
        { "ever", true },
        { "every", true },
        { "everyone", true },
        { "everything", true },
        { "everywhere", true },
        { "except", true },
        { "few", true },
        { "fifteen", true },
        { "fify", true },
        { "fill", true },
        { "find", true },
        { "fire", true },
        { "first", true },
        { "five", true },
        { "for", true },
        { "former", true },
        { "formerly", true },
        { "forty", true },
        { "found", true },
        { "four", true },
        { "from", true },
        { "front", true },
        { "full", true },
        { "further", true },
        { "get", true },
        { "give", true },
        { "go", true },
        { "had", true },
        { "has", true },
        { "have", true },
        { "he", true },
        { "hence", true },
        { "her", true },
        { "here", true },
        { "hereafter", true },
        { "hereby", true },
        { "herein", true },
        { "hereupon", true },
        { "hers", true },
        { "herself", true },
        { "him", true },
        { "himself", true },
        { "his", true },
        { "how", true },
        { "however", true },
        { "hundred", true },
        { "i", true },
        { "ie", true },
        { "if", true },
        { "in", true },
        { "inc", true },
        { "indeed", true },
        { "interest", true },
        { "into", true },
        { "is", true },
        { "it", true },
        { "its", true },
        { "itself", true },
        { "keep", true },
        { "last", true },
        { "latter", true },
        { "latterly", true },
        { "least", true },
        { "less", true },
        { "ltd", true },
        { "made", true },
        { "many", true },
        { "may", true },
        { "me", true },
        { "meanwhile", true },
        { "might", true },
        { "mill", true },
        { "mine", true },
        { "more", true },
        { "moreover", true },
        { "most", true },
        { "mostly", true },
        { "move", true },
        { "much", true },
        { "must", true },
        { "my", true },
        { "myself", true },
        { "name", true },
        { "namely", true },
        { "neither", true },
        { "never", true },
        { "nevertheless", true },
        { "next", true },
        { "nine", true },
        { "no", true },
        { "nobody", true },
        { "none", true },
        { "nor", true },
        { "not", true },
        { "nothing", true },
        { "now", true },
        { "nowhere", true },
        { "of", true },
        { "off", true },
        { "often", true },
        { "on", true },
        { "once", true },
        { "one", true },
        { "only", true },
        { "onto", true },
        { "or", true },
        { "other", true },
        { "others", true },
        { "otherwise", true },
        { "our", true },
        { "ours", true },
        { "ourselves", true },
        { "out", true },
        { "over", true },
        { "own", true },
        { "part", true },
        { "per", true },
        { "perhaps", true },
        { "please", true },
        { "put", true },
        { "rather", true },
        { "re", true },
        { "same", true },
        { "see", true },
        { "seem", true },
        { "seemed", true },
        { "seeming", true },
        { "seems", true },
        { "serious", true },
        { "several", true },
        { "she", true },
        { "should", true },
        { "show", true },
        { "side", true },
        { "since", true },
        { "sincere", true },
        { "six", true },
        { "sixty", true },
        { "so", true },
        { "some", true },
        { "somehow", true },
        { "someone", true },
        { "something", true },
        { "sometime", true },
        { "sometimes", true },
        { "somewhere", true },
        { "still", true },
        { "such", true },
        { "system", true },
        { "take", true },
        { "ten", true },
        { "than", true },
        { "that", true },
        { "the", true },
        { "their", true },
        { "them", true },
        { "themselves", true },
        { "then", true },
        { "thence", true },
        { "there", true },
        { "thereafter", true },
        { "thereby", true },
        { "therefore", true },
        { "therein", true },
        { "thereupon", true },
        { "these", true },
        { "they", true },
        { "thick", true },
        { "thin", true },
        { "third", true },
        { "this", true },
        { "those", true },
        { "though", true },
        { "three", true },
        { "through", true },
        { "throughout", true },
        { "thru", true },
        { "thus", true },
        { "to", true },
        { "together", true },
        { "too", true },
        { "top", true },
        { "toward", true },
        { "towards", true },
        { "twelve", true },
        { "twenty", true },
        { "two", true },
        { "un", true },
        { "under", true },
        { "until", true },
        { "up", true },
        { "upon", true },
        { "us", true },
        { "very", true },
        { "via", true },
        { "was", true },
        { "we", true },
        { "well", true },
        { "were", true },
        { "what", true },
        { "whatever", true },
        { "when", true },
        { "whence", true },
        { "whenever", true },
        { "where", true },
        { "whereafter", true },
        { "whereas", true },
        { "whereby", true },
        { "wherein", true },
        { "whereupon", true },
        { "wherever", true },
        { "whether", true },
        { "which", true },
        { "while", true },
        { "whither", true },
        { "who", true },
        { "whoever", true },
        { "whole", true },
        { "whom", true },
        { "whose", true },
        { "why", true },
        { "will", true },
        { "with", true },
        { "within", true },
        { "without", true },
        { "would", true },
        { "yet", true },
        { "you", true },
        { "your", true },
        { "yours", true },
        { "yourself", true },
        { "yourselves", true }
    };

    /// <summary>
    /// Chars that separate words.
    /// </summary>
    static char[] _delimiters = new char[]
    {
        ' ',
        ',',
        ';',
        '.'
    };

    /// <summary>
    /// Remove stopwords from string.
    /// </summary>
    public static string RemoveStopwords(string input)
    {
        // 1
        // Split parameter into words
        var words = input.Split(_delimiters,
            StringSplitOptions.RemoveEmptyEntries);
        // 2
        // Allocate new dictionary to store found words
        var found = new Dictionary<string, bool>();
        // 3
        // Store results in this StringBuilder
        StringBuilder builder = new StringBuilder();
        // 4
        // Loop through all words
        foreach (string currentWord in words)
        {
            // 5
            // Convert to lowercase
            string lowerWord = currentWord.ToLower();
            // 6
            // If this is a usable word, add it
            if (!_stops.ContainsKey(lowerWord) &&
                !found.ContainsKey(lowerWord))
            {
                builder.Append(currentWord).Append(' ');
                found.Add(lowerWord, true);
            }
        }
        // 7
        // Return string with words removed
        return builder.ToString().Trim();
    }
    }
}