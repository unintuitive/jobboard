using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JobBoard.Data;
using JobBoard.Models;

namespace JobBoard.Services
{
    public interface IClassificationService
    {
        Task<Classification[]> GetClassificationsAsync();
        Task ClassifyAsync(Guid id);
    }
}