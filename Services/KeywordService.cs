using System;
using System.Linq;
using System.Threading.Tasks;
using JobBoard.Data;
using JobBoard.Models;
using Microsoft.EntityFrameworkCore;

namespace JobBoard.Services
{
    public class KeywordService : IKeywordService
    {
        private readonly ApplicationDbContext _context;

        public KeywordService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Keyword[]> GetKeywordsAsync()
        {
            return await Keyword.SQLKeywordsAsync(_context);
        }
    }
}