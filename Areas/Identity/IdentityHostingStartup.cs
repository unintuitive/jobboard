using System;
using JobBoard.Areas.Identity.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(JobBoard.Areas.Identity.IdentityHostingStartup))]
namespace JobBoard.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<JobBoardIdentityDbContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("JobBoardIdentityDbContextConnection")));

                services
                    .AddDefaultIdentity<IdentityUser>()
                    .AddRoles<IdentityRole>()
                    .AddEntityFrameworkStores<JobBoardIdentityDbContext>();
            });
        }
    }
}