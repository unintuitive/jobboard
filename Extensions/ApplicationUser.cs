using Microsoft.AspNetCore.Identity;

namespace JobBoard.Extensions
{
    public class ApplicationUser : IdentityUser
    {
        public TYPE Type { get; set; }
    }
}